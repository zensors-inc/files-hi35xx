/*********************************************************************************
*
*  Copyright (C) 2014 Hisilicon Technologies Co., Ltd.  All rights reserved. 
*
*  This program is confidential and proprietary to Hisilicon Technologies Co., Ltd.
*  (Hisilicon), and may not be copied, reproduced, modified, disclosed to
*  others, published or used, in whole or in part, without the express prior
*  written permission of Hisilicon.
*
***********************************************************************************/
#include <asm/setup.h>
#include <asm/barrier.h>    /* mb() */
#include <asm/uaccess.h>
#include <asm/system.h>
#include <linux/string.h>
#include <linux/slab.h>
#include <linux/errno.h>
#include <linux/miscdevice.h>
#include <linux/fcntl.h>
#include <linux/param.h>
#include <linux/delay.h>

#include <linux/init.h>
#include <asm/io.h>
#include <linux/delay.h>
#include <linux/proc_fs.h>
#include <linux/workqueue.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/list.h>
#include <linux/wait.h>
#include <linux/spinlock.h>
#include <linux/version.h>
#include <linux/interrupt.h>
#include <linux/time.h>
#include <linux/random.h>
#include <linux/signal.h>
#include <linux/seq_file.h>

#include "hi_type.h"
#include "hi_drv_cipher.h"
#include "drv_hash.h"
#include "hi_type.h"
#include "drv_cipher_define.h"
#include "drv_cipher_ioctl.h"
#include "hal_cipher.h"
#include "drv_cipher.h"


#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif

/* Set the defualt timeout value for hash calculating (5000 ms)*/
#define HASH_MAX_DURATION (5000)
#define HASH_PADDING_LEGNTH     8

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,36)
DECLARE_MUTEX(g_HashMutexKernel);
#else
DEFINE_SEMAPHORE(g_HashMutexKernel);
#endif

typedef enum
{
    HASH_READY,
    REC_READY,
    DMA_READY,
}HASH_WAIT_TYPE;

inline HI_S32 HASH_WaitReady(HI_UNF_CIPHER_HASH_TYPE_E enHashType,
							HASH_WAIT_TYPE enType,
							HI_BOOL bRecRdy)		/* only for REC_READY, 0: use rec_len1, 1:use rec_rdy */
{
    CIPHER_SHA_STATUS_U unCipherSHAstatus;
    HI_SIZE_T ulStartTime = 0;
    HI_SIZE_T ulLastTime = 0;
    HI_SIZE_T ulDuraTime = 0;

    /* wait for hash_rdy */
    ulStartTime = jiffies;
    while(1)
    {
        unCipherSHAstatus.u32 = 0;
        (HI_VOID)HAL_CIPHER_ReadReg(CIPHER_HASH_REG_STATUS_ADDR, &unCipherSHAstatus.u32);
        if(HASH_READY == enType)
        {
            if(1 == unCipherSHAstatus.bits.hash_rdy)
            {
                break;
            }
        }
        else if (REC_READY == enType)
        {
            if(1 == unCipherSHAstatus.bits.rec_rdy)
            {
                break;
            }
        }
        else if (DMA_READY == enType)
        {
            if(1 == unCipherSHAstatus.bits.dma_rdy)
            {
                break;
            }
        }
        else
        {
            HI_ERR_CIPHER("Error! Invalid wait type!\n");
            return HI_FAILURE;
        }

        ulLastTime = jiffies;
        ulDuraTime = jiffies_to_msecs(ulLastTime - ulStartTime);
        if (ulDuraTime >= HASH_MAX_DURATION )
        { 
            HI_ERR_CIPHER("Error! Hash time out!\n");
            return HI_FAILURE;
        }

		msleep(1);
    }

    return HI_SUCCESS;
}

HI_S32 HAL_Hash_Init(CIPHER_HASH_DATA_S *pCipherHashData)
{
    HI_S32 ret = HI_SUCCESS;
    CIPHER_SHA_CTRL_U unCipherSHACtrl;
    CIPHER_SHA_START_U unCipherSHAStart;
    HI_U32 i;

    if( NULL == pCipherHashData )
    {
        HI_ERR_CIPHER("Error! Null pointer input!\n");
        (HI_VOID)HAL_Cipher_HashSoftReset();
        return HI_FAILURE;
    }

    /* wait for hash_rdy */
    ret = HASH_WaitReady(pCipherHashData->enShaType, HASH_READY, 0);
    if(HI_SUCCESS != ret)
    {
        HI_ERR_CIPHER("Hash wait ready failed!\n");
        (HI_VOID)HAL_Cipher_HashSoftReset();
        return HI_FAILURE;
    }

#if 0
    /* set hmac-sha key */
    if( ((HI_UNF_CIPHER_HASH_TYPE_HMAC_SHA1 == pCipherHashData->enShaType) || (HI_UNF_CIPHER_HASH_TYPE_HMAC_SHA256 == pCipherHashData->enShaType))
        && (HI_CIPHER_HMAC_KEY_FROM_CPU == pCipherHashData->enHMACKeyFrom) )
    {
        for( i = 0; i < CIPHER_HMAC_KEY_LEN; i = i + 4)
        {
            u32WriteData = (pCipherHashData->pu8HMACKey[3+i] << 24) |
                           (pCipherHashData->pu8HMACKey[2+i] << 16) |
                           (pCipherHashData->pu8HMACKey[1+i] << 8)  |
                           (pCipherHashData->pu8HMACKey[0+i]);
            (HI_VOID)HAL_CIPHER_WriteReg(CIPHER_HASH_REG_MCU_KEY0 + i, u32WriteData);
        }
    }
#endif

    /* write total len low and high */
    (HI_VOID)HAL_CIPHER_WriteReg(CIPHER_HASH_REG_TOTALLEN_LOW_ADDR, pCipherHashData->u32InputDataLen);
    (HI_VOID)HAL_CIPHER_WriteReg(CIPHER_HASH_REG_TOTALLEN_HIGH_ADDR, 0);

    /* config sha_ctrl : read by dma first, and by cpu in the hash final function */
    unCipherSHACtrl.u32 = 0;
    (HI_VOID)HAL_CIPHER_ReadReg(CIPHER_HASH_REG_CTRL_ADDR, &unCipherSHACtrl.u32);
    unCipherSHACtrl.bits.read_ctrl = 0;
    if( HI_UNF_CIPHER_HASH_TYPE_SHA1 == pCipherHashData->enShaType )
    {
        unCipherSHACtrl.bits.hardkey_hmac_flag = 0;
        unCipherSHACtrl.bits.sha_sel= 0x0;
    }
    else if( HI_UNF_CIPHER_HASH_TYPE_SHA256 == pCipherHashData->enShaType )
    {
        unCipherSHACtrl.bits.hardkey_hmac_flag = 0;
        unCipherSHACtrl.bits.sha_sel= 0x1;
    }
    else if( HI_UNF_CIPHER_HASH_TYPE_HMAC_SHA1 == pCipherHashData->enShaType )
    {
		/* unCipherSHACtrl.bits.hardkey_hmac_flag = 1; */
        unCipherSHACtrl.bits.hardkey_hmac_flag = 0;
        unCipherSHACtrl.bits.sha_sel= 0x0;
		/* unCipherSHACtrl.bits.hardkey_sel = pCipherHashData->enHMACKeyFrom; */
    }
    else if( HI_UNF_CIPHER_HASH_TYPE_HMAC_SHA256 == pCipherHashData->enShaType )
    {
        unCipherSHACtrl.bits.hardkey_hmac_flag = 0;
        unCipherSHACtrl.bits.sha_sel= 0x1;
		/* unCipherSHACtrl.bits.hardkey_sel = pCipherHashData->enHMACKeyFrom; */
    }
    else
    {
        HI_ERR_CIPHER("Invalid hash type input!\n");
        (HI_VOID)HAL_Cipher_HashSoftReset();
        return HI_FAILURE;
    }
    unCipherSHACtrl.bits.small_end_en = 1;
    if (pCipherHashData->bIsUseIinitVar)
    {
        unCipherSHACtrl.bits.sha_init_update_en = 1;
        for(i=0; i<8; i++)
        {
            (HI_VOID)HAL_CIPHER_WriteReg(CIPHER_HASH_REG_INIT1_UPDATE+i*4, pCipherHashData->u32InitVar[i]);
        }
    }
    else
    {
         unCipherSHACtrl.bits.sha_init_update_en = 0;
    }

    (HI_VOID)HAL_CIPHER_WriteReg(CIPHER_HASH_REG_CTRL_ADDR, unCipherSHACtrl.u32);    
    
    /* config sha_start */
    unCipherSHAStart.u32 = 0;
    unCipherSHAStart.bits.sha_start = 1;
    (HI_VOID)HAL_CIPHER_WriteReg(CIPHER_HASH_REG_START_ADDR, unCipherSHAStart.u32);

    return HI_SUCCESS;
}

HI_S32 HAL_Hash_Update(CIPHER_HASH_DATA_S *pCipherHashData)
{
    HI_S32 ret = HI_SUCCESS;

    if( NULL == pCipherHashData )
    {
        HI_ERR_CIPHER("Error, Null pointer input!\n");
        (HI_VOID)HAL_Cipher_HashSoftReset();
        return HI_FAILURE;
    }

    if (pCipherHashData->u32InputDataLen == 0)
    {
        return HI_SUCCESS;
    }

    ret= HASH_WaitReady(pCipherHashData->enShaType, REC_READY, 0);
    if(HI_SUCCESS != ret)
    {
        HI_ERR_CIPHER("Hash wait ready failed!\n");
        (HI_VOID)HAL_Cipher_HashSoftReset();
        return HI_FAILURE;
    }

    (HI_VOID)HAL_CIPHER_WriteReg(CIPHER_HASH_REG_DMA_START_ADDR, pCipherHashData->stMMZBuffer.u32StartPhyAddr);
    (HI_VOID)HAL_CIPHER_WriteReg(CIPHER_HASH_REG_DMA_LEN, pCipherHashData->u32InputDataLen);

    ret = HASH_WaitReady(pCipherHashData->enShaType, REC_READY, 0);
    if(HI_SUCCESS != ret)
    {
        HI_ERR_CIPHER("Hash wait ready failed!\n");
        (HI_VOID)HAL_Cipher_HashSoftReset();
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

static HI_U32 HAL_Hash_Small2Large(HI_U32 u32SamllVal)
{
    HI_U32 u32LargeVal = 0;

    u32LargeVal  = (u32SamllVal >> 24) & 0xFF;
    u32LargeVal |= ((u32SamllVal >> 16) & 0xFF) << 8;
    u32LargeVal |= ((u32SamllVal >> 8) & 0xFF) << 16;
    u32LargeVal |= ((u32SamllVal) & 0xFF) << 24;

    return u32LargeVal;
}

HI_S32 HAL_Hash_Final(CIPHER_HASH_DATA_S *pCipherHashData)
{
    HI_S32 ret = HI_SUCCESS;
    CIPHER_SHA_STATUS_U unCipherSHAStatus;
	HI_U32 sha_out[8];
    HI_U32 i = 0;


    if( NULL == pCipherHashData )
    {
        HI_ERR_CIPHER("Error, Null pointer input!\n");
        (HI_VOID)HAL_Cipher_HashSoftReset();
        return HI_FAILURE;
    }

    /* wait for rec_ready instead of hash_ready */
    ret= HASH_WaitReady(pCipherHashData->enShaType, HASH_READY, 1);
    if(HI_SUCCESS != ret)
    {
        HI_ERR_CIPHER("Hash wait ready failed\n");
        (HI_VOID)HAL_Cipher_HashSoftReset();
        return HI_FAILURE;
    }

    /* read digest */
    unCipherSHAStatus.u32 = 0;
    (HI_VOID)HAL_CIPHER_ReadReg(CIPHER_HASH_REG_STATUS_ADDR, &unCipherSHAStatus.u32);

    if( (0x00 == unCipherSHAStatus.bits.error_state) && (0x00 == unCipherSHAStatus.bits.len_err))
    {
        memset(sha_out, 0x0, sizeof(sha_out));
        if( (HI_UNF_CIPHER_HASH_TYPE_SHA1 == pCipherHashData->enShaType)
         || (HI_UNF_CIPHER_HASH_TYPE_HMAC_SHA1 == pCipherHashData->enShaType))
        {
    		(HI_VOID)HAL_CIPHER_ReadReg(CIPHER_HASH_REG_SHA_OUT1, &(sha_out[0]));
    		(HI_VOID)HAL_CIPHER_ReadReg(CIPHER_HASH_REG_SHA_OUT2, &(sha_out[1]));
    		(HI_VOID)HAL_CIPHER_ReadReg(CIPHER_HASH_REG_SHA_OUT3, &(sha_out[2]));
    		(HI_VOID)HAL_CIPHER_ReadReg(CIPHER_HASH_REG_SHA_OUT4, &(sha_out[3]));
    		(HI_VOID)HAL_CIPHER_ReadReg(CIPHER_HASH_REG_SHA_OUT5, &(sha_out[4]));

            if (pCipherHashData->pu8Output != HI_NULL)
    		{
        		for(i = 0; i < 5; i++)
        		{
        		    /* small endian */
        			pCipherHashData->pu8Output[i * 4 + 3] = sha_out[i] >> 24;
        			pCipherHashData->pu8Output[i * 4 + 2] = sha_out[i] >> 16;
        			pCipherHashData->pu8Output[i * 4 + 1] = sha_out[i] >> 8;
        			pCipherHashData->pu8Output[i * 4]     = sha_out[i];
        		}
            }

            for(i = 0; i < 5; i++)
            {
                pCipherHashData->u32InitVar[i] = HAL_Hash_Small2Large(sha_out[i]);
            }
        }
        else if( (HI_UNF_CIPHER_HASH_TYPE_SHA256 == pCipherHashData->enShaType )
              || (HI_UNF_CIPHER_HASH_TYPE_HMAC_SHA256 == pCipherHashData->enShaType))
        {
    		(HI_VOID)HAL_CIPHER_ReadReg(CIPHER_HASH_REG_SHA_OUT1, &(sha_out[0]));
    		(HI_VOID)HAL_CIPHER_ReadReg(CIPHER_HASH_REG_SHA_OUT2, &(sha_out[1]));
    		(HI_VOID)HAL_CIPHER_ReadReg(CIPHER_HASH_REG_SHA_OUT3, &(sha_out[2]));
    		(HI_VOID)HAL_CIPHER_ReadReg(CIPHER_HASH_REG_SHA_OUT4, &(sha_out[3]));
    		(HI_VOID)HAL_CIPHER_ReadReg(CIPHER_HASH_REG_SHA_OUT5, &(sha_out[4]));
    		(HI_VOID)HAL_CIPHER_ReadReg(CIPHER_HASH_REG_SHA_OUT6, &(sha_out[5]));
    		(HI_VOID)HAL_CIPHER_ReadReg(CIPHER_HASH_REG_SHA_OUT7, &(sha_out[6]));
    		(HI_VOID)HAL_CIPHER_ReadReg(CIPHER_HASH_REG_SHA_OUT8, &(sha_out[7]));

            if (pCipherHashData->pu8Output != HI_NULL)
    		{
        		for(i = 0; i < 8; i++)
        		{
        		    /* small endian */
        			pCipherHashData->pu8Output[i * 4 + 3] = sha_out[i] >> 24;
        			pCipherHashData->pu8Output[i * 4 + 2] = sha_out[i] >> 16;
        			pCipherHashData->pu8Output[i * 4 + 1] = sha_out[i] >> 8;
        			pCipherHashData->pu8Output[i * 4]     = sha_out[i];
        		}
            }

            for(i = 0; i < 8; i++)
            {
                pCipherHashData->u32InitVar[i] = HAL_Hash_Small2Large(sha_out[i]);
            }
        }
        else
        {
            HI_ERR_CIPHER("Invalid hash type : %d!\n", pCipherHashData->enShaType);
            (HI_VOID)HAL_Cipher_HashSoftReset();
            return HI_FAILURE;
        }
    }
    else
    {
        HI_ERR_CIPHER("Error! SHA Status Reg: error_state = %d!\n", unCipherSHAStatus.bits.error_state);
        HI_ERR_CIPHER("Error! SHA Status Reg: len_err = %d!\n", unCipherSHAStatus.bits.len_err);
        (HI_VOID)HAL_Cipher_HashSoftReset();
        return HI_FAILURE;
    }

    (HI_VOID)HAL_Cipher_HashSoftReset();

    return HI_SUCCESS;
}

HI_S32 HAL_Cipher_HashSoftReset(HI_VOID)
{
    HI_U32 CipherCrgValue;
    HI_U32 pvirt;

    pvirt = (HI_U32)ioremap_nocache(REG_SYS_SHA_CLK_ADDR_PHY, 16);

    HAL_CIPHER_ReadReg(pvirt, &CipherCrgValue);       
    HAL_SET_BIT(CipherCrgValue, 2); /* reset */ 
    HAL_SET_BIT(CipherCrgValue, 3); /* set the bit 3, clock opened */    
    HAL_CIPHER_WriteReg(pvirt,CipherCrgValue);    

    /* clock select and cancel reset 0x30100*/    
    HAL_CLEAR_BIT(CipherCrgValue, 2); /* cancel reset */    
    HAL_SET_BIT(CipherCrgValue, 3);   /* set the bit 1, clock opened */        
    HAL_CIPHER_WriteReg(pvirt,CipherCrgValue); 

    iounmap((void __iomem *)pvirt);
    
    return HI_SUCCESS;
}

HI_S32 Cipher_CalcHashInit_Hw(CIPHER_HASH_DATA_S *pCipherHashData)
{
	if(NULL == pCipherHashData)
	{
        HI_ERR_CIPHER("Invalid param, null pointer!\n");
        return HI_FAILURE;
	}

    switch(pCipherHashData->enShaType)
    {
        case HI_UNF_CIPHER_HASH_TYPE_SHA1:
        case HI_UNF_CIPHER_HASH_TYPE_HMAC_SHA1:
            pCipherHashData->u32InitVar[0] = 0x67452301;
            pCipherHashData->u32InitVar[1] = 0xefcdab89;
            pCipherHashData->u32InitVar[2] = 0x98badcfe;
            pCipherHashData->u32InitVar[3] = 0x10325476;
            pCipherHashData->u32InitVar[4] = 0xc3d2e1f0;
			pCipherHashData->bIsUseIinitVar = HI_TRUE;
            break;
        case HI_UNF_CIPHER_HASH_TYPE_SHA256:
        case HI_UNF_CIPHER_HASH_TYPE_HMAC_SHA256:
            pCipherHashData->u32InitVar[0] = 0x6a09e667;
            pCipherHashData->u32InitVar[1] = 0xbb67ae85;
            pCipherHashData->u32InitVar[2] = 0x3c6ef372;
            pCipherHashData->u32InitVar[3] = 0xa54ff53a;
            pCipherHashData->u32InitVar[4] = 0x510e527f;
            pCipherHashData->u32InitVar[5] = 0x9b05688c;
            pCipherHashData->u32InitVar[6] = 0x1f83d9ab;
            pCipherHashData->u32InitVar[7] = 0x5be0cd19;
			pCipherHashData->bIsUseIinitVar = HI_TRUE;
            break;
        default:
            pCipherHashData->bIsUseIinitVar = HI_FALSE;
            break;
    }

    return HI_SUCCESS;
}

static HI_S32 Cipher_CalcHashUpdate_Hw(CIPHER_HASH_DATA_S *pCipherHashData)
{
    HI_S32 ret = HI_SUCCESS;

    if( NULL == pCipherHashData )
    {
        HI_ERR_CIPHER("Error, Null pointer input!\n");
        return HI_FAILURE;
    }

    ret = HAL_Hash_Init(pCipherHashData);
    if(HI_SUCCESS != ret)
    {
        HI_ERR_CIPHER("HAL_Hash_Init failed! ret = 0x%08x\n", ret);
        return HI_FAILURE;
    }
    
    ret = HAL_Hash_Update(pCipherHashData);
    if(HI_SUCCESS != ret)
    {
        HI_ERR_CIPHER("HAL_Hash_Update failed! ret = 0x%08x\n", ret);
        return HI_FAILURE;
    }

    pCipherHashData->bIsUseIinitVar = HI_TRUE;

    ret = HAL_Hash_Final(pCipherHashData);
    if(HI_SUCCESS != ret)
    {
        HI_ERR_CIPHER("HAL_Hash_Final failed! ret = 0x%08x\n", ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

HI_S32 Cipher_CalcHashFinal_Hw(CIPHER_HASH_DATA_S *pCipherHashData)
{
    HI_S32 ret = HI_SUCCESS;

    if( NULL == pCipherHashData )
    {
        HI_ERR_CIPHER("Error, Null pointer input!\n");
        return HI_FAILURE;
    }

	ret = HAL_Hash_Init(pCipherHashData);
    if(HI_SUCCESS != ret)
    {
        HI_ERR_CIPHER("HAL_Hash_Init failed! ret = 0x%08x\n", ret);
        return HI_FAILURE;
    }
    
    ret = HAL_Hash_Update(pCipherHashData);
    if(HI_SUCCESS != ret)
    {
        HI_ERR_CIPHER("HAL_Hash_Update failed! ret = 0x%08x\n", ret);
        return HI_FAILURE;
    }

    ret = HAL_Hash_Final(pCipherHashData);
    if(HI_SUCCESS != ret)
    {
        HI_ERR_CIPHER("HAL_Hash_Final failed! ret = 0x%08x\n", ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

HI_S32 HI_DRV_CIPHER_CalcHashInit(CIPHER_HASH_DATA_S *pCipherHashData)
{
	HI_S32 ret = HI_SUCCESS;

    if(down_interruptible(&g_HashMutexKernel))
    {
    	HI_ERR_CIPHER("down_interruptible failed!\n");
        return HI_FAILURE;
    }

    ret = Cipher_CalcHashInit_Hw(pCipherHashData);

	up(&g_HashMutexKernel);
	return ret;
}

HI_S32 HI_DRV_CIPHER_CalcHashUpdate(CIPHER_HASH_DATA_S *pCipherHashData)
{
	HI_S32 ret = HI_SUCCESS;

    if(down_interruptible(&g_HashMutexKernel))
    {
    	HI_ERR_CIPHER("down_interruptible failed!\n");
        return HI_FAILURE;
    }

    ret = Cipher_CalcHashUpdate_Hw(pCipherHashData);

	up(&g_HashMutexKernel);
	return ret;
}

HI_S32 HI_DRV_CIPHER_CalcHashFinal(CIPHER_HASH_DATA_S *pCipherHashData)
{
	HI_S32 ret = HI_SUCCESS;

    if(down_interruptible(&g_HashMutexKernel))
    {
    	HI_ERR_CIPHER("down_interruptible failed!\n");
        return HI_FAILURE;
    }

    ret = Cipher_CalcHashFinal_Hw(pCipherHashData);

	up(&g_HashMutexKernel);
	return ret;
}

HI_VOID HASH_DRV_ModInit(HI_VOID)
{
    return ;
}

#ifdef MODULE
//module_init(HASH_DRV_ModInit);
//module_exit(HASH_DRV_ModExit);
#endif

EXPORT_SYMBOL(HI_DRV_CIPHER_CalcHashInit);
EXPORT_SYMBOL(HI_DRV_CIPHER_CalcHashUpdate);
EXPORT_SYMBOL(HI_DRV_CIPHER_CalcHashFinal);


MODULE_AUTHOR("Hi3720 MPP GRP");
MODULE_LICENSE("GPL");

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif
