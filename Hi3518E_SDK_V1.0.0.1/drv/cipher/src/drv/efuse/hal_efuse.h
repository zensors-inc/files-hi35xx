#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/fcntl.h>
#include <linux/mm.h>
#include <linux/proc_fs.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <asm/uaccess.h>
#include <mach/hardware.h>
#include <asm/io.h>
#include <asm/system.h>
#include <linux/miscdevice.h>
#include <linux/delay.h>
#include "hi_type.h"
#include <linux/proc_fs.h>
#include <linux/poll.h>

#include <asm/bitops.h>
#include <asm/uaccess.h>
#include <asm/irq.h>

#include <linux/moduleparam.h>
#include <linux/ioport.h>
#include <linux/interrupt.h>

HI_S32 HAL_Efuse_WriteKey(HI_U32 * p_key, HI_U32 opt_id);
HI_S32 HAL_Efuse_LoadCipherKey(HI_U32 chn_id, HI_U32 opt_id);
HI_S32 HAL_Efuse_LoadHashKey(HI_U32 opt_id);
HI_VOID HAL_Efuse_Init(HI_VOID);
