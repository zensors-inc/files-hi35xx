/******************************************************************************

  Copyright (C), 2011-2014, Hisilicon Tech. Co., Ltd.

 ******************************************************************************
  File Name     : drv_cipher_intf.c
  Version       : Initial Draft
  Author        : Hisilicon hisecurity team
  Created       : 
  Last Modified :
  Description   : 
  Function List :
  History       :
******************************************************************************/
#include <linux/proc_fs.h>
#include <linux/module.h>
#include <linux/signal.h>
#include <linux/spinlock.h>
#include <linux/personality.h>
#include <linux/ptrace.h>
#include <linux/kallsyms.h>
#include <linux/init.h>
#include <linux/pci.h>
#include <linux/seq_file.h>
#include <asm/atomic.h>
#include <asm/cacheflush.h>
#include <asm/io.h>
#include <asm/system.h>
#include <asm/uaccess.h>
#include <asm/unistd.h>
#include <asm/traps.h>
#include <linux/miscdevice.h>

#include "hi_type.h"
#include "hal_cipher.h"
#include "drv_cipher.h"
#include "drv_cipher_ioctl.h"
#include "drv_hash.h"
#include "drv_cipher_log.h"
#include "hi_drv_cipher.h"


#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* End of #ifdef __cplusplus */


extern HI_VOID DRV_CIPHER_UserCommCallBack(HI_U32 arg);

HI_U32 g_u32CipherBase = 0;

HI_VOID HAL_CIPHER_ReadReg(HI_U32 addr, HI_U32 *pu32Val)
{
    HI_REG_READ32(addr, *pu32Val);
    return;
}

HI_VOID HAL_CIPHER_WriteReg(HI_U32 addr, HI_U32 u32Val)
{
    HI_REG_WRITE32(addr, u32Val);
    return;
}

HI_S32 CIPHER_Ioctl(struct inode *inode, struct file *file, unsigned int cmd, HI_VOID *argp)
{
    HI_S32 ret = HI_SUCCESS;

    switch(cmd)
    {
#ifdef CFG_HI_CIPHER_MULTICIPHER_SUPPORT
	    case CMD_CIPHER_CREATEHANDLE:
		{
	        CIPHER_HANDLE_S *pstCIHandle = (CIPHER_HANDLE_S *)argp;
            
			ret = HI_DRV_CIPHER_CreateHandle(pstCIHandle, (HI_U32)file);
            
	        break;
	    }
	
	    case CMD_CIPHER_DESTROYHANDLE:
	    {
	        HI_HANDLE hCipherChn = *(HI_HANDLE *)argp;

			ret = HI_DRV_CIPHER_DestroyHandle(hCipherChn);
	        break;
	    }
	    case CMD_CIPHER_CONFIGHANDLE:
	    {
	        CIPHER_Config_CTRL stCIConfig = *(CIPHER_Config_CTRL *)argp;

	        HI_U32 softChnId = HI_HANDLE_GET_CHNID(stCIConfig.CIHandle);  
	        ret = HI_DRV_CIPHER_ConfigChn(softChnId, &stCIConfig.CIpstCtrl);
	        break;
	    }
	    case CMD_CIPHER_ENCRYPT:
	    {
	        CIPHER_DATA_S *pstCIData = (CIPHER_DATA_S *)argp;

			ret = HI_DRV_CIPHER_Encrypt(pstCIData);
			break;
	    }
	    case CMD_CIPHER_DECRYPT:
	    {
	        CIPHER_DATA_S *pstCIData = (CIPHER_DATA_S *)argp;

			ret = HI_DRV_CIPHER_Decrypt(pstCIData);
			break;
	    }
	    case CMD_CIPHER_ENCRYPTMULTI:
	    {
	        CIPHER_DATA_S *pstCIData = (CIPHER_DATA_S *)argp;

			ret = HI_DRV_CIPHER_EncryptMulti(pstCIData);
			break;
	    }
	    case CMD_CIPHER_DECRYPTMULTI:
	    {
	        CIPHER_DATA_S *pstCIData = (CIPHER_DATA_S *)argp;
    
			ret = HI_DRV_CIPHER_DecryptMulti(pstCIData);
	        break;
	    }
#ifdef CFG_HI_CIPHER_RNG_SUPPORT
	    case CMD_CIPHER_GETRANDOMNUMBER:
	    {
	        CIPHER_RNG_S *pstRNG  = (CIPHER_RNG_S *)argp;
			ret = HI_DRV_CIPHER_GetRandomNumber(pstRNG);
	        break;
	    }
#endif
	    case CMD_CIPHER_GETHANDLECONFIG:
	    {
	        CIPHER_Config_CTRL *pstCIData = (CIPHER_Config_CTRL *)argp;

	        ret = HI_DRV_CIPHER_GetHandleConfig(pstCIData);
	        break;
	    }
#endif

#ifdef CFG_HI_CIPHER_HASH_SUPPORT
	    case CMD_CIPHER_CALCHASHINIT:
	    {
	        CIPHER_HASH_DATA_S *pstCipherHashData = (CIPHER_HASH_DATA_S*)argp;
            pstCipherHashData->enHMACKeyFrom = HI_CIPHER_HMAC_KEY_FROM_CPU;
	        ret = HI_DRV_CIPHER_CalcHashInit(pstCipherHashData);
	        break;
	    }
	    case CMD_CIPHER_CALCHASHUPDATE:
	    {
	        CIPHER_HASH_DATA_S *pstCipherHashData = (CIPHER_HASH_DATA_S*)argp;

	        ret = HI_DRV_CIPHER_CalcHashUpdate(pstCipherHashData);
	        break;
	    }
	    case CMD_CIPHER_CALCHASHFINAL:
	    {
	        CIPHER_HASH_DATA_S *pstCipherHashData = (CIPHER_HASH_DATA_S*)argp;

	        ret = HI_DRV_CIPHER_CalcHashFinal(pstCipherHashData);
	        break;
	    }
#endif
#ifdef CFG_HI_CIPHER_RSA_SUPPORT
        case CMD_CIPHER_CALCRSA:
        {
	        CIPHER_RSA_DATA_S *pCipherRsaData = (CIPHER_RSA_DATA_S*)argp;
	        ret = HI_DRV_CIPHER_CalcRsa(pCipherRsaData);
	        break;
	    }       
#endif
	    default:
	    {
	        HI_ERR_CIPHER("Error: Inappropriate ioctl for device. cmd=%d\n", cmd);
	        ret = HI_FAILURE;
	        break;
	    }
    }

    return ret;
}

static long DRV_CIPHER_Ioctl(struct file *ffile, unsigned int cmd, unsigned long arg)
{
    long ret;
    CMD_CIPHER_PARAM_U unCmdParam;

    if(_IOC_SIZE(cmd) > sizeof(unCmdParam))
    {            
        HI_ERR_CIPHER("Invalid cmd param size %d!\n", _IOC_SIZE(cmd));            
        return HI_ERR_CIPHER_INVALID_PARA;                   
    }

    if (copy_from_user(&unCmdParam, (void __user *)arg, _IOC_SIZE(cmd)))        
    {            
        HI_ERR_CIPHER("copy data from user fail!\n");            
        return HI_FAILURE;                   
    }

    ret = CIPHER_Ioctl(ffile->f_dentry->d_inode, ffile, cmd, (HI_VOID*)&unCmdParam);

    if (copy_to_user((void __user *)arg, &unCmdParam, _IOC_SIZE(cmd)))        
    {            
        HI_ERR_CIPHER("copy data to user fail!\n");            
        return HI_FAILURE;        
    }

    return ret;
}

/** <* ref from linux/fs.h @by g00182102 */
static struct file_operations DRV_CIPHER_Fops=
{
    .owner            = THIS_MODULE,
#if defined(CFG_HI_CIPHER_MULTICIPHER_SUPPORT)
    .open             = DRV_CIPHER_Open,
#endif
    .unlocked_ioctl   = DRV_CIPHER_Ioctl,
#if defined(CFG_HI_CIPHER_MULTICIPHER_SUPPORT)
    .release          = DRV_CIPHER_Release,
#endif
};

static struct miscdevice cipher_dev = 
{       
    .minor      = MISC_DYNAMIC_MINOR,    
    .name       = UMAP_DEVNAME_CIPHER,    
    .fops       = &DRV_CIPHER_Fops,
};

HI_S32 CIPHER_DRV_ModInit(HI_VOID)
{
    HI_S32 ret = HI_SUCCESS;
    
   if (misc_register(&cipher_dev))    
   {        
       HI_ERR_CIPHER("ERROR: could not register cipher devices\n");      
       return -1;    
   }

   g_u32CipherBase = (HI_U32)ioremap_nocache(REG_CI_BASE_ADDR_PHY, REG_CI_BASE_SIZE);
    
#ifdef CFG_HI_CIPHER_MULTICIPHER_SUPPORT 
    ret = DRV_CIPHER_Init();
    if (HI_SUCCESS != ret)
    {
        return ret;
    }
#endif

#ifdef CFG_HI_CIPHER_HASH_SUPPORT 
    (HI_VOID)HAL_Cipher_HashSoftReset();
#endif

    HI_PRINT("Load hi_cipher.ko success.\n");

    return HI_SUCCESS;
}

HI_VOID CIPHER_DRV_ModExit(HI_VOID)
{
#ifdef CFG_HI_CIPHER_MULTICIPHER_SUPPORT
    (HI_VOID)DRV_CIPHER_DeInit();
#endif

    misc_deregister(&cipher_dev);   

    if (g_u32CipherBase != 0)    
    {        
        iounmap((void __iomem *)g_u32CipherBase);        
        g_u32CipherBase = 0;   
    }
    HI_PRINT("Unload hi_cipher.ko success.\n");
}

#ifdef MODULE
module_init(CIPHER_DRV_ModInit);
module_exit(CIPHER_DRV_ModExit);
#endif

MODULE_AUTHOR("Hi3720 MPP GRP");
MODULE_LICENSE("GPL");

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */

